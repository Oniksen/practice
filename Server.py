#!/usr/bin/env python
import time
from bottle import route, request, run, template 

name = None
operation = None

@route('/')
def index():
    global name, operation, operation_time
    name = request.query.name or name
    operation = request.query.operation or operation
    curtime = time.ctime()

    f = open('text.txt', 'a')
    f.write("\n{}\t{}\t{}\n".format(name,operation,curtime))
    f.close()
    return('{"status": "Идет операция","status2":"Операция завершена","status3":"Закончите предыдущую операцию"}')
 
run(host='localhost', port=8000)
